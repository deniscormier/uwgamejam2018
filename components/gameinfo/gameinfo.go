package gameinfo

import "engo.io/engo"

type GameInfoComponent struct {
	Speed               float32
	Direction           engo.Point
	Health              int
	InvincibilityPeriod float32
	Animation           string
}
