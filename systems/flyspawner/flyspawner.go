package flyspawner

import (
	"log"
	"math/rand"

	"engo.io/ecs"
	"engo.io/engo"

	"bitbucket.org/deniscormier/uwgamejam2018/entities/fly"
	bee_s "bitbucket.org/deniscormier/uwgamejam2018/systems/bee"
)

type flySpawnerEntry struct {
	point     engo.Point
	direction string
}

type FlySpawnerSystem struct {
	w              *ecs.World
	flySpawnPoints []flySpawnerEntry
	nextSpawn      float32

	BeeSystem *bee_s.BeeSystem
}

func (fsys *FlySpawnerSystem) New(w *ecs.World) {
	fsys.w = w
	fsys.flySpawnPoints = []flySpawnerEntry{}
	fsys.nextSpawn = rand.Float32()
}

func (fsys *FlySpawnerSystem) AddSpawnPoint(point engo.Point, direction string) {
	fsys.flySpawnPoints = append(fsys.flySpawnPoints, flySpawnerEntry{point, direction})
}

// Doesn't make sense to implement this here
func (fsys *FlySpawnerSystem) Remove(basic ecs.BasicEntity) {}

func (fsys *FlySpawnerSystem) Update(dt float32) {
	fsys.nextSpawn -= dt
	if fsys.nextSpawn <= 0 {
		r := rand.Intn(len(fsys.flySpawnPoints))
		randomSpawnPoint := fsys.flySpawnPoints[r].point

		log.Printf("New fly: %+v", randomSpawnPoint)
		// TODO flies could be going in the wrong direction
		f := fly.NewFly(randomSpawnPoint, fsys.flySpawnPoints[r].direction)
		fsys.w.AddEntity(f)
		fsys.BeeSystem.AddFly(&f.BasicEntity, &f.SpaceComponent, &f.GameInfoComponent)

		fsys.nextSpawn = rand.Float32()
	}
}
