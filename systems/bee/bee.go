package bee

import (
	"log"

	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
	"engo.io/engo/math"

	"bitbucket.org/deniscormier/uwgamejam2018/components/gameinfo"
	"bitbucket.org/deniscormier/uwgamejam2018/entities/bee/health"
)

type beeEntity struct {
	*ecs.BasicEntity
	*common.SpaceComponent
	*common.AnimationComponent
	*gameinfo.GameInfoComponent
}

type flyEntity struct {
	*ecs.BasicEntity
	*common.SpaceComponent
	*gameinfo.GameInfoComponent
}

type BeeSystem struct {
	w            *ecs.World
	bee          beeEntity
	beeHealthBar *health.HealthBar
	flies        []flyEntity
}

func (bsys *BeeSystem) New(w *ecs.World) {
	bsys.w = w

	// On collision against a fly, must take action
	engo.Mailbox.Listen("CollisionMessage", func(message engo.Message) {

		collision, isCollision := message.(common.CollisionMessage)
		if isCollision {

			// Figure out if (and which) collision property is our bee
			switch bsys.bee.BasicEntity.ID() {
			case collision.Entity.BasicEntity.ID():
				bsys.beeCollisionWith(collision.To.BasicEntity.ID())
			case collision.To.BasicEntity.ID():
				bsys.beeCollisionWith(collision.Entity.BasicEntity.ID())
			default:
				log.Printf("Collision does not concern the bee: %+v")
			}
		}
	})

	bsys.flies = []flyEntity{}
}

func (bsys *BeeSystem) AddBee(basic *ecs.BasicEntity, sc *common.SpaceComponent, anim *common.AnimationComponent, gi *gameinfo.GameInfoComponent) {
	bsys.bee = beeEntity{
		BasicEntity:        basic,
		SpaceComponent:     sc,
		AnimationComponent: anim,
		GameInfoComponent:  gi,
	}
}

func (bsys *BeeSystem) AddHealthBar(pos engo.Point) {
	bsys.beeHealthBar = health.NewHealthBar(bsys.w, pos, 3)
}

func (bsys *BeeSystem) AddFly(basic *ecs.BasicEntity, sc *common.SpaceComponent, gi *gameinfo.GameInfoComponent) {
	bsys.flies = append(bsys.flies, flyEntity{
		BasicEntity:       basic,
		SpaceComponent:    sc,
		GameInfoComponent: gi,
	})
}

func (bsys *BeeSystem) Remove(basic ecs.BasicEntity) {
	if bsys.bee.BasicEntity.ID() == basic.ID() {
		bsys.bee = beeEntity{}
		return
	}

	for i, f := range bsys.flies {
		if f.BasicEntity.ID() == basic.ID() {
			bsys.flies = append(bsys.flies[:i], bsys.flies[i+1:]...)
			return
		}
	}
}

func (bsys *BeeSystem) Update(dt float32) {
	if engo.Input.Button("quit").JustPressed() {
		log.Println("You pressed escape. Closing the game.")
		engo.Exit()
	}

	// Reduce bee's invincibility period if above zero
	if iPeriod := bsys.bee.GameInfoComponent.InvincibilityPeriod; iPeriod > 0 {
		bsys.bee.GameInfoComponent.InvincibilityPeriod = math.Max(iPeriod-dt, 0)
	}

	// Calculate each fly's displacement given their speed/direction
	for _, f := range bsys.flies {
		f.SpaceComponent.Position.X += f.GameInfoComponent.Direction.X * f.GameInfoComponent.Speed * dt
		f.SpaceComponent.Position.Y += f.GameInfoComponent.Direction.Y * f.GameInfoComponent.Speed * dt
	}

	// Move the bee based on arrows pressed
	hAxis := engo.Input.Axis(engo.DefaultHorizontalAxis).Value()
	vAxis := engo.Input.Axis(engo.DefaultVerticalAxis).Value()

	// Determine if the bee changes animation state
	g := bsys.bee.GameInfoComponent
	if anim := nextAnimation(g.Direction.X, g.InvincibilityPeriod, g.Health); anim != g.Animation {
		//log.Print("New bee animation state: " + anim)
		g.Animation = anim
		bsys.bee.AnimationComponent.SelectAnimationByName(anim)
	}

	// If the bee is dead, move bee down until impact with the ground (early return)
	if g.Animation == "dead" {
		bsys.bee.SpaceComponent.Position.Y += bsys.bee.GameInfoComponent.Direction.Y * bsys.bee.GameInfoComponent.Speed * dt
		return
	}

	// Perform bee displacement
	if hAxis != 0 {
		bsys.bee.SpaceComponent.Position.X += hAxis * bsys.bee.GameInfoComponent.Speed * dt
		bsys.bee.GameInfoComponent.Direction.X = hAxis
	}

	if vAxis != 0 {
		bsys.bee.SpaceComponent.Position.Y += vAxis * bsys.bee.GameInfoComponent.Speed * dt
		bsys.bee.GameInfoComponent.Direction.Y = vAxis
	}
}

func (bsys *BeeSystem) beeCollisionWith(id uint64) {
	// If the bee is invincible from a previous hit, return immediately
	if bsys.bee.GameInfoComponent.InvincibilityPeriod > 0 {
		return
	}

	// If the id matches with a fly, the bee has hit a fly and takes damage
	for _, f := range bsys.flies {
		if f.BasicEntity.ID() == id {
			bsys.bee.GameInfoComponent.Health--
			bsys.beeHealthBar.SetHealth(bsys.bee.GameInfoComponent.Health)
			if bsys.bee.GameInfoComponent.Health <= 0 {
				bsys.bee.GameInfoComponent.InvincibilityPeriod = 1 // prevents log spamming
				bsys.bee.GameInfoComponent.Direction.Y = 1
				log.Print("The bee is dead. Game over.")
			} else {
				bsys.bee.GameInfoComponent.InvincibilityPeriod = 2
				log.Printf("The bee takes damage. Current health: %d", bsys.bee.GameInfoComponent.Health)
			}
			return
		}
	}

	// Given we've collided with something else, if the bee is dead, it should stop moving
	if bsys.bee.GameInfoComponent.Health <= 0 {
		bsys.bee.GameInfoComponent.Direction.Y = 0
	}
}

func nextAnimation(xDir float32, invincibilityPeriod float32, health int) string {
	if health <= 0 {
		return "dead"
	}

	// Combine bee state and direction
	state := "flying"
	if invincibilityPeriod > 0 {
		state = "takingDamage"
	}

	direction := "Right"
	// Expected to be -1 or 1
	if xDir < 0 {
		direction = "Left"
	}

	return state + direction
}
