package background

import (
	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
)

type Background struct {
	ecs.BasicEntity
	common.RenderComponent
	common.SpaceComponent
}

func NewBackground(tileElement *common.Tile) common.Renderable {
	return &Background{
		BasicEntity: ecs.NewBasic(),
		RenderComponent: common.RenderComponent{
			Drawable: tileElement,
			Scale:    engo.Point{1, 1},
		},
		SpaceComponent: common.SpaceComponent{
			Position: tileElement.Point,
			Width:    70,
			Height:   70,
		},
	}
}
