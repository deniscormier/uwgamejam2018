package bee

import (
	"log"

	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"

	"bitbucket.org/deniscormier/uwgamejam2018/components/gameinfo"
	"bitbucket.org/deniscormier/uwgamejam2018/systems/collision"
)

type Bee struct {
	ecs.BasicEntity
	common.RenderComponent
	common.SpaceComponent
	common.CollisionComponent
	common.AnimationComponent
	gameinfo.GameInfoComponent
}

var (
	files = map[string]string{
		"flying1Left":        "images/70_70/bee/flying1Left.png",
		"flying2Left":        "images/70_70/bee/flying2Left.png",
		"hurtLeft":           "images/70_70/bee/hurtLeft.png",
		"flying1Right":       "images/70_70/bee/flying1Right.png",
		"flying2Right":       "images/70_70/bee/flying2Right.png",
		"hurtRight":          "images/70_70/bee/hurtRight.png",
		"invincibilityFrame": "images/70_70/bee/invincibilityFrame.png",
		"dead":               "images/70_70/bee/dead.png",
	}
	flyingLeft        = "flyingLeft"
	flyingRight       = "flyingRight"
	takingDamageLeft  = "takingDamageLeft"
	takingDamageRight = "takingDamageRight"
	dead              = "dead"
)

// Called at scene setup
func FileNames() []string {
	names := []string{}
	for _, v := range files {
		names = append(names, v)
	}
	return names
}

func NewBee(pos engo.Point) *Bee {
	textures := map[string]common.Drawable{}
	var err error
	for k, v := range files {
		textures[k], err = common.LoadedSprite(v)
		if err != nil {
			log.Fatalf("Unable to load %s, err: %s", v, err)
		}
	}

	flyingLeftAction := &common.Animation{Name: flyingLeft, Frames: []int{0, 0, 1, 1}, Loop: true}
	flyingRightAction := &common.Animation{Name: flyingRight, Frames: []int{3, 3, 4, 4}, Loop: true}
	takingDamageLeftAction := &common.Animation{Name: takingDamageLeft, Frames: []int{2, 6, 1, 6, 2, 6, 1, 6, 2, 6, 1, 6, 2, 6, 1, 6}}   // 2-second animation
	takingDamageRightAction := &common.Animation{Name: takingDamageRight, Frames: []int{5, 6, 4, 6, 5, 6, 4, 6, 5, 6, 4, 6, 5, 6, 4, 6}} // 2-second animation
	deadAction := &common.Animation{Name: dead, Frames: []int{7}, Loop: true}
	actions := []*common.Animation{flyingLeftAction, flyingRightAction, takingDamageLeftAction, takingDamageRightAction, deadAction}

	anim := common.NewAnimationComponent([]common.Drawable{
		textures["flying1Left"],
		textures["flying2Left"],
		textures["hurtLeft"],
		textures["flying1Right"],
		textures["flying2Right"],
		textures["hurtRight"],
		textures["invincibilityFrame"],
		textures["dead"],
	}, 0.125) // 8 FPS
	anim.AddAnimations(actions)
	anim.AddDefaultAnimation(flyingRightAction)

	b := &Bee{
		BasicEntity: ecs.NewBasic(),
		RenderComponent: common.RenderComponent{
			Drawable: textures["flying1Right"],
			Scale:    engo.Point{1, 1},
		},
		SpaceComponent: common.SpaceComponent{
			Width:  textures["flying1Right"].Width(),
			Height: textures["flying1Right"].Height(),
		},
		CollisionComponent: common.CollisionComponent{
			Main: collision.Level | collision.Enemy | collision.Nectar,
		},
		AnimationComponent: anim,
		GameInfoComponent: gameinfo.GameInfoComponent{
			Speed:     150,
			Health:    3,
			Direction: engo.Point{1, 0},
			Animation: flyingRight,
		},
	}

	b.SpaceComponent.SetCenter(pos)
	b.RenderComponent.SetZIndex(2)
	return b
}
