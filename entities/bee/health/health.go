package health

import (
	"log"

	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
)

var (
	heartEmptyFileName         = "images/70_70/HUD/hud_heartEmpty.png"
	heartFullFileName          = "images/70_70/HUD/hud_heartFull.png"
	heartOffset        float32 = 60
)

// Called at scene setup
func FileNames() []string {
	return []string{heartEmptyFileName, heartFullFileName}
}

type Heart struct {
	ecs.BasicEntity
	common.RenderComponent
	common.SpaceComponent
	heartEmptyTexture *common.Texture
	heartFullTexture  *common.Texture
}

func newHeart(pos engo.Point) *Heart {
	h := &Heart{BasicEntity: ecs.NewBasic()}

	var err error
	h.heartEmptyTexture, err = common.LoadedSprite(heartEmptyFileName)
	if err != nil {
		log.Fatalf("Unable to load %s, err: %s", heartEmptyFileName, err)
	}

	h.heartFullTexture, err = common.LoadedSprite(heartFullFileName)
	if err != nil {
		log.Fatalf("Unable to load %s, err: %s", heartFullFileName, err)
	}

	h.RenderComponent = common.RenderComponent{
		Scale:    engo.Point{1, 1},
		Drawable: h.heartFullTexture,
	}

	h.SpaceComponent = common.SpaceComponent{
		Width:  h.heartFullTexture.Width(),
		Height: h.heartFullTexture.Height(),
	}

	h.RenderComponent.SetZIndex(3)
	h.SpaceComponent.SetCenter(pos)

	log.Printf("New heart: %+v", h.SpaceComponent)
	return h
}

func (h *Heart) Full(isFull bool) {
	if isFull {
		h.RenderComponent.Drawable = h.heartFullTexture
	} else {
		h.RenderComponent.Drawable = h.heartEmptyTexture
	}
}

type HealthBar struct {
	position engo.Point
	hearts   []*Heart
}

func NewHealthBar(w *ecs.World, pos engo.Point, health int) *HealthBar {
	hb := &HealthBar{
		position: pos,
		hearts:   []*Heart{},
	}

	originalPosX := pos.X

	// Right-to-left facilitates the calculations in SetHealth a bit
	for i := health - 1; i >= 0; i-- {
		pos.X = originalPosX + float32(i)*heartOffset

		h := newHeart(pos)
		hb.hearts = append(hb.hearts, h)
		w.AddEntity(h)
	}

	return hb
}

func (hb *HealthBar) SetHealth(health int) {
	// If health == 1... true (rightmost heart full), false, false, ...
	// If health == 2... true (rightmost heart full), true, false, ...
	// ...
	for i, h := range hb.hearts {
		if i < health {
			h.Full(true)
		} else {
			h.Full(false)
		}
	}
}
