package fly

import (
	"log"
	"math/rand"

	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"

	"bitbucket.org/deniscormier/uwgamejam2018/components/gameinfo"
	"bitbucket.org/deniscormier/uwgamejam2018/systems/collision"
)

type Fly struct {
	ecs.BasicEntity
	common.RenderComponent
	common.SpaceComponent
	common.CollisionComponent
	common.AnimationComponent
	gameinfo.GameInfoComponent
}

var (
	files = map[string]string{
		"flying1Left":  "images/70_70/fly/flying1Left.png",
		"flying2Left":  "images/70_70/fly/flying2Left.png",
		"flying1Right": "images/70_70/fly/flying1Right.png",
		"flying2Right": "images/70_70/fly/flying2Right.png",
	}
	flyingLeft  = "flyingLeft"
	flyingRight = "flyingRight"
)

// Called at scene setup
func FileNames() []string {
	names := []string{}
	for _, v := range files {
		names = append(names, v)
	}
	return names
}

func NewFly(pos engo.Point, flyType string) *Fly {
	textures := map[string]common.Drawable{}
	var err error
	for k, v := range files {
		textures[k], err = common.LoadedSprite(v)
		if err != nil {
			log.Fatalf("Unable to load %s, err: %s", v, err)
		}
	}

	flyingLeftAction := &common.Animation{Name: flyingLeft, Frames: []int{0, 0, 1, 1}, Loop: true}
	flyingRightAction := &common.Animation{Name: flyingRight, Frames: []int{2, 2, 3, 3}, Loop: true}
	actions := []*common.Animation{flyingLeftAction}

	anim := common.NewAnimationComponent([]common.Drawable{
		textures["flying1Left"],
		textures["flying2Left"],
		textures["flying1Right"],
		textures["flying2Right"],
	}, 0.125) // 8 FPS
	anim.AddAnimations(actions)

	// Random vertical direction between [-1, 1)
	// After normalization, range of angles is 45 degrees up to 45 degrees down
	randomVerticalDirection := rand.Float32()*2 - 1

	// Choose appropriate animation with relation to the direction
	var direction engo.Point
	switch flyType {
	case "fly-left":
		p := engo.Point{-1, randomVerticalDirection}
		normalized, _ := p.Normalize()
		direction = normalized
		anim.AddDefaultAnimation(flyingLeftAction)
	case "fly-right":
		p := engo.Point{1, randomVerticalDirection}
		normalized, _ := p.Normalize()
		direction = normalized
		anim.AddDefaultAnimation(flyingRightAction)
	default:
		log.Fatal("Invalid fly type: " + flyType)
	}

	b := &Fly{
		BasicEntity: ecs.NewBasic(),
		RenderComponent: common.RenderComponent{
			Drawable: textures["flying1Left"],
			Scale:    engo.Point{1, 1},
		},
		SpaceComponent: common.SpaceComponent{
			Width:  textures["flying1Left"].Width(),
			Height: textures["flying1Left"].Height(),
		},
		CollisionComponent: common.CollisionComponent{
			Group: collision.Enemy,
		},
		AnimationComponent: anim,
		GameInfoComponent: gameinfo.GameInfoComponent{
			Speed:     100,
			Direction: direction,
		},
	}
	b.SpaceComponent.SetCenter(pos)
	b.RenderComponent.SetZIndex(1)
	return b
}
