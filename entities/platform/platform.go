package platform

import (
	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"

	"bitbucket.org/deniscormier/uwgamejam2018/systems/collision"
)

type Platform struct {
	ecs.BasicEntity
	common.RenderComponent
	common.SpaceComponent
	common.CollisionComponent
}

func NewPlatform(tileElement *common.Tile) common.Renderable {
	return &Platform{
		BasicEntity: ecs.NewBasic(),
		RenderComponent: common.RenderComponent{
			Drawable: tileElement,
			Scale:    engo.Point{1, 1},
		},
		SpaceComponent: common.SpaceComponent{
			Position: tileElement.Point,
			Width:    70,
			Height:   70,
		},
		CollisionComponent: common.CollisionComponent{
			Group: collision.Level,
		},
	}
}
