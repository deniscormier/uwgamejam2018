package nectar

import (
	"log"

	"bitbucket.org/deniscormier/uwgamejam2018/systems/collision"
	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
)

var (
	files = map[string]string{
		"yellowCrystal1": "images/70_70/plants/yellowCrystal1.png",
		"yellowCrystal2": "images/70_70/plants/yellowCrystal2.png",
		"yellowCrystal3": "images/70_70/plants/yellowCrystal3.png",
		"yellowCrystal4": "images/70_70/plants/yellowCrystal4.png",
		"yellowCrystal5": "images/70_70/plants/yellowCrystal5.png",
	}
	pulse = "pulse"
)

// Called at scene setup
func FileNames() []string {
	names := []string{}
	for _, v := range files {
		names = append(names, v)
	}
	return names
}

type Nectar struct {
	ecs.BasicEntity
	common.RenderComponent
	common.SpaceComponent
	common.CollisionComponent
	common.AnimationComponent
}

func NewNectar(pos engo.Point) *Nectar {
	textures := map[string]common.Drawable{}
	var err error
	for k, v := range files {
		textures[k], err = common.LoadedSprite(v)
		if err != nil {
			log.Fatalf("Unable to load %s, err: %s", v, err)
		}
	}

	pulseAction := &common.Animation{Name: pulse, Frames: []int{0, 1, 2, 3, 4, 3, 2, 1, 0, 0}, Loop: true}
	actions := []*common.Animation{pulseAction}

	anim := common.NewAnimationComponent([]common.Drawable{
		textures["yellowCrystal1"],
		textures["yellowCrystal2"],
		textures["yellowCrystal3"],
		textures["yellowCrystal4"],
		textures["yellowCrystal5"],
	}, 0.083) // 12 FPS
	anim.AddAnimations(actions)
	anim.AddDefaultAnimation(pulseAction)

	n := &Nectar{
		BasicEntity: ecs.NewBasic(),
		RenderComponent: common.RenderComponent{
			Scale:    engo.Point{1, 1},
			Drawable: textures["yellowCrystal1"],
		},
		SpaceComponent: common.SpaceComponent{
			Width:  textures["yellowCrystal1"].Width(),
			Height: textures["yellowCrystal1"].Height(),
		},
		CollisionComponent: common.CollisionComponent{
			Group: collision.Nectar,
		},
		AnimationComponent: anim,
	}

	n.RenderComponent.SetZIndex(1)
	n.SpaceComponent.SetCenter(pos)

	return n
}
