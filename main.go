//go:generate go-bindata -pkg assets -o assets/assets.go assets/...
package main

import (
	"math/rand"
	"time"

	"bitbucket.org/deniscormier/uwgamejam2018/scenes/game"
	"engo.io/engo"
)

func main() {
	// Initialize seed for random number generation
	rand.Seed(time.Now().Unix())

	opts := engo.RunOptions{
		Title:  "Be the Bee you Want to Be",
		Width:  840,
		Height: 560,
	}

	engo.Run(opts, &game.Game{
		LevelFileName: "levels/test.tmx",
	})
}
