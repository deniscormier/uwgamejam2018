package game

import (
	"log"

	"engo.io/ecs"
	"engo.io/engo"
	"engo.io/engo/common"
	"golang.org/x/image/colornames"

	"bitbucket.org/deniscormier/uwgamejam2018/entities/background"
	bee_e "bitbucket.org/deniscormier/uwgamejam2018/entities/bee"
	"bitbucket.org/deniscormier/uwgamejam2018/entities/bee/health"
	fly_e "bitbucket.org/deniscormier/uwgamejam2018/entities/fly"
	"bitbucket.org/deniscormier/uwgamejam2018/entities/nectar"
	"bitbucket.org/deniscormier/uwgamejam2018/entities/platform"
	bee_s "bitbucket.org/deniscormier/uwgamejam2018/systems/bee"
	"bitbucket.org/deniscormier/uwgamejam2018/systems/collision"
	"bitbucket.org/deniscormier/uwgamejam2018/systems/flyspawner"
)

type Game struct {
	LevelFileName    string
	beeSystem        *bee_s.BeeSystem
	flySpawnerSystem *flyspawner.FlySpawnerSystem
}

func (game *Game) Type() string { return "Game" }

func (game *Game) Preload() {
	if game.LevelFileName == "" {
		log.Fatal("Need to provide a level for the game to load")
	}

	files := []string{game.LevelFileName}
	files = append(files, bee_e.FileNames()...)
	files = append(files, fly_e.FileNames()...)
	files = append(files, health.FileNames()...)
	files = append(files, nectar.FileNames()...)

	if err := engo.Files.Load(files...); err != nil {
		panic(err)
	}

}

func (game *Game) Setup(u engo.Updater) {
	w, _ := u.(*ecs.World)

	common.SetBackground(colornames.Skyblue)

	// Register keys
	engo.Input.RegisterAxis(engo.DefaultHorizontalAxis, engo.AxisKeyPair{engo.KeyArrowLeft, engo.KeyArrowRight})
	engo.Input.RegisterAxis(engo.DefaultVerticalAxis, engo.AxisKeyPair{engo.KeyArrowUp, engo.KeyArrowDown})
	engo.Input.RegisterButton("quit", engo.KeyEscape)
	//engo.Input.RegisterButton("action", engo.KeySpace, engo.KeyEnter)

	// Add systems to the world
	var renderable *common.Renderable
	w.AddSystemInterface(&common.RenderSystem{}, renderable, nil)
	var collisionable *common.Collisionable
	w.AddSystemInterface(&common.CollisionSystem{Solids: collision.Level}, collisionable, nil)
	var animationable *common.Animationable
	w.AddSystemInterface(&common.AnimationSystem{}, animationable, nil)

	// These systems won't automatically receive an entity added through w.AddEntity(...)
	game.beeSystem = &bee_s.BeeSystem{}
	w.AddSystem(game.beeSystem)
	game.flySpawnerSystem = &flyspawner.FlySpawnerSystem{
		BeeSystem: game.beeSystem,
	}
	w.AddSystem(game.flySpawnerSystem)

	// Load level
	resource, err := engo.Files.Resource(game.LevelFileName)
	if err != nil {
		panic(err)
	}
	tmxResource := resource.(common.TMXResource)
	levelData := tmxResource.Level

	for _, tileLayer := range levelData.TileLayers {

		switch name := tileLayer.Name; name {

		// The "Platform" layer is all the same type of solid square blocks
		case "Platforms":
			for _, tileElement := range tileLayer.Tiles {
				if tileElement.Image != nil {

					log.Printf("New platform: %+v", tileElement)

					p := platform.NewPlatform(tileElement)

					w.AddEntity(p)
				}
			}
			log.Print("Tile layer has been processed: " + name)

		// The "Background tiles" layer tiles won't have any collision component
		case "Background tiles":
			for _, tileElement := range tileLayer.Tiles {
				if tileElement.Image != nil {

					log.Printf("New background tile: %+v", tileElement)

					p := background.NewBackground(tileElement)

					w.AddEntity(p)
				}
			}
			log.Print("Tile layer has been processed: " + name)

		default:
			log.Print("Skipped tile layer: " + name)
		}
	}

	// Do the same for all image layers
	for _, imageLayer := range levelData.ImageLayers {
		log.Print("Skipped image layer: " + imageLayer.Name)
	}

	// Access Object Layers
	for _, objectLayer := range levelData.ObjectLayers {

		switch name := objectLayer.Name; name {

		// The "Spawn points" layer contains many points of interest
		case "Spawn points":

			for _, object := range objectLayer.Objects {

				switch t := object.Name; t {

				case "Bee spawn point":
					pos := engo.Point{
						float32(object.X),
						float32(object.Y),
					}
					b := bee_e.NewBee(pos)

					w.AddEntity(b)
					game.beeSystem.AddBee(&b.BasicEntity, &b.SpaceComponent, &b.AnimationComponent, &b.GameInfoComponent)

					log.Printf("New Bee: %+v", object)

				case "Bee health bar":
					pos := engo.Point{
						float32(object.X),
						float32(object.Y),
					}

					// w.AddEntity happens inside the call
					game.beeSystem.AddHealthBar(pos)

					log.Printf("New Bee health bar: %+v", object)

				case "Fly spawn point":
					pos := engo.Point{
						float32(object.X),
						float32(object.Y),
					}
					b := fly_e.NewFly(pos, object.Type)

					w.AddEntity(b)
					game.beeSystem.AddFly(&b.BasicEntity, &b.SpaceComponent, &b.GameInfoComponent)

					game.flySpawnerSystem.AddSpawnPoint(pos, object.Type)
					log.Printf("New Fly: %+v", object)

				case "Nectar spawn point":
					pos := engo.Point{
						float32(object.X),
						float32(object.Y),
					}
					n := nectar.NewNectar(pos)

					w.AddEntity(n)
					//game.beeSystem.AddFly(&b.BasicEntity, &b.SpaceComponent, &b.GameInfoComponent)

					log.Printf("New Nectar: %+v", object)

				default:
					log.Print("Unrecognized object: " + object.Name)
				}
			}

			log.Print("Object layer has been processed: " + name)

		// The "Background objects" layer objects won't have collision components
		case "Background objects":

			for _, object := range objectLayer.Objects {

				t := &common.Tile{
					Image: levelData.Tileset[object.Id].Image,
					Point: engo.Point{
						float32(object.X),
						float32(object.Y),
					},
				}
				p := platform.NewPlatform(t)

				w.AddEntity(p)

				log.Printf("New object: %+v", object)
			}

			log.Print("Object layer has been processed: " + name)

		default:
			log.Print("Skipped object layer: " + name)
		}
	}

	log.Printf("Setup complete: " + game.Type())
}
