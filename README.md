# uwgamejam2018

Worst case Ontario, this will my first complete game jam!

## Build and run

```sh
git clone git@bitbucket.org:deniscormier/uwgamejam2018.git
# Optionally, run
# git clone https://deniscormier@bitbucket.org/deniscormier/uwgamejam2018.git

go get ./...
# Unusable quite sadly :(
#go get github.com/go-bindata/go-bindata/...

# go generate

go build main.go

./main
```

## Why can't I use go-bindata to put all my asset data in the binary?

The Engo framework presents `func (formats *Formats) LoadReaderData(url string, f io.Reader) error` which means we can provide asset data through an io.Reader using `bytes.NewReader(binaryAssetData)`.

Here is what I was hoping to accomplish:

```go
files := []string{game.LevelFileName}
	files = append(files, bee_e.FileNames()...)
	files = append(files, fly_e.FileNames()...)
	files = append(files, health.FileNames()...)
	files = append(files, nectar.FileNames()...)

	for _, file := range files {
		fmt.Println(file)
		b, err := assets.Asset(file)
		if err != nil {
			panic(err)
		}

		r := bytes.NewReader(b)
		if err := engo.Files.LoadReaderData(file, r); err != nil {
			panic(err)
		}
	}
```

The major roadblock comes from parsing `.tmx` files...

```go
Formats.LoadReaderData(url string, f io.Reader)
calls tmxLoader.Load(url string, data io.Reader)
calls createLevelFromTmx(tmxBytes []byte, tmxURL string)
calls getTextureResourcesFromTmx
calls engo.Files.Load(url)
```

And guess what? `engo.Files.Load` attempts to load files referenced in the `.tmx` file and we can't prevent that function from going to disk :(